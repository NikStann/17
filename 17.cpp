
#include <iostream>
#include <vector>
#include <cmath>
#include <regex>
#include <string>
using namespace std;

class Vector
{
private:
    double i=3;
    double n=4;
    double v;
public:
    int GetI()
    {
        return i;
    }
    int GetN()
    {
        return n;
    }
    int GetV()
    {
        double v = sqrt(pow(i,2) + pow(n,2));
        return v;
    }
};

int main()
{
    Vector temp;
    cout << temp.GetI() << "\n";
    cout << temp.GetN() << "\n";
    cout << temp.GetV();
}


